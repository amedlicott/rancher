version: '2'
services:
  gw:
    image: kong:0.10.2
    environment:
      - KONG_DATABASE=postgres
      - KONG_PG_HOST=kong-database
      - KONG_PG_DATABASE=kong
    expose:
      - 8000
      - 8001
      - 8443
      - 8444
    ports:
      - "8000:8000"
    links:
      - kong-database

  kong-database:
    image: postgres:9.4
    environment:
      - POSTGRES_USER=kong
      - POSTGRES_DB=kong
    volumes:
      - "db-data-kong-postgres:/var/lib/postgresql/data"

  kong-dashboard:
    image: pgbi/kong-dashboard
    ports:
      - "8080:8080"