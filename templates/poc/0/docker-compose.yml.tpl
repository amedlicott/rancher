version: '2'
services:
  web:
    image: 166519038888.dkr.ecr.eu-west-1.amazonaws.com/digitialmlpoc:latest
    ports:
     - "5000:5000"
  redis:
    image: "redis:alpine"