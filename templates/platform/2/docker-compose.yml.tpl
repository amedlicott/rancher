version: '2'
services:
  web:
    image: office.digitalml.com:8888/ignite-wildfly:6.0.2.0
    ports:
      - 80:8080
      - 443:8443
    links:
      - ignitees
      - ignitedb
      - igniteftp
      - igniteldap
    environment:
      - LOGGING_SERVICE=http://ignitees:9200
      - DB_USERNAME=ignite
      - DB_PASSWORD=Adm!n123
      - DB_PASSWORD_ENCRYPTED=ifuMOVGM0BtT5niL0TrT1g==
      - DB_ENCRYPTED_PASSWORDS=true
      - IGNITE_MASTER=true
      - DB_BACKGROUND_VALIDATION=true
      - DB_BACKGROUND_VALIDATION_MILLIS=300000
      - IDP_url=
      - LOGOUT_url=
      - LOGIN_TYPE=other
      - EntityID=https://localhost:443/ICS/
      - ICS_url=https://localhost:443/ICS/
      - trust.domains=localhost
      - SIGNATURES=false
      - BINDING_TYPE=none
      - DB_IDLE_TIMEOUT_MINUTES=15
      - DB_CONNECTION_URL=jdbc:oracle:thin:@ignitedb:1521:XE
      - DB_ORGANISER_CONNECTION_URL=DB_CONNECTION_URL
      - DB_EXTERNAL_CONNECTION_URL=jdbc:oracle:thin:@127.0.0.1:2521:XE
      - DB_DRIVER_CLASS=oracle.jdbc.OracleDriver
      - DB_DRIVER=oracle
      - DBURL=http://ignitedb:8080/apex
      - DB_VALID_CONNECTION_CHECKER=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleValidConnectionChecker
      - DB_EXCEPTION_SORTER=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleExceptionSorter
      - KEY.url=/opt/jboss/wildfly/standalone/configuration/server.keystore
      - KeyStorePass=store123
      - Alias=servercert
      - SigningAlias=servercert
      - IDP.domain=localhost
      - picketlink.audit.enable=true


  igniteftp:
    image: atmoz/sftp
    environment:
      - FTP_USERNAME
      - FTP_PASSWORD
      - FTP_ROOT
    command: foo:pass:::poller,artefact

  ignitees:
    image: office.digitalml.com:8888/ignite-elasticsearch:6.0.2.0
    environment:
      - cluster.name=ignite-cluster
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ports:
      - 9200:9200
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
    cap_add:
      - IPC_LOCK


  ignitedb:
    image: office.digitalml.com:8888/ignite-oracle:6.0.2.0
    volumes:
      - /u01/app/oracle


  igniteldap:
    image: office.digitalml.com:8888/ignite-ldap:6.0.2.0



  setup:
    image: office.digitalml.com:8888/ignite-setup-insurance:6.0.2.0
    links:
      - web
    environment:
      - SETUP_NAME="Ignite Standard Insurance Demo"
      - BASE_URL=http://web:8080/ICS
